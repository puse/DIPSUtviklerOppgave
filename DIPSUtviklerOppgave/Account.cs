﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIPSUtviklerOppgave
{
    public class Account : IComparable<Account>
    {
        private Money balance;
        private string accountName;
        private int accountID;
        public Person Owner;

        public Account(Person owner, Money initialDeposit, string accountName, int accountID)
        {
            this.Owner = owner;
            this.accountID = accountID;
            this.accountName = accountName;
            this.balance = initialDeposit;
        }

        /**
         * @brief This method withdraws an amount from the balance of the account.
         * It will throw an exception if the withdrawn amount is negative, and or 
         * zero. It will also throw an exception if the account has insufficient
         * funds.
         * 
         */
        public void Withdraw(Money amount)
        {
            if (amount.Amount <= 0)
            {
                throw new BankException(BankException.AmountLessOrEqualToZeroMessage);
            }

            if (balance.CompareTo(amount) < 0)
            {
                throw new BankException(BankException.InsufficientFundsMessage);
            }

            balance.Amount -= amount.Amount;
        }

        /**
         *  @breif This method will deposit the amount given into this account.
         *  The method will throw an exception if the deposited amount is zero, 
         *  and or negative.
         * 
         */
        public void Deposit(Money amount)
        {
            if (amount.Amount <= 0)
            {
                throw new BankException(BankException.AmountLessOrEqualToZeroMessage);
            }

            balance.Amount += amount.Amount;
        }

        public int CompareTo(Account other)
        {
            return this.balance.CompareTo(other.GetBalance());
        }

        public string GetAccountName()
        {
            return accountName;
        }

        public Money GetBalance()
        {
            return balance;
        }
    }
}
