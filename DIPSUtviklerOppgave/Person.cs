﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIPSUtviklerOppgave
{
    public class Person
    {
        private string firstname;
        private string lastname;
        private int nextAccountID = 1;
        private List<Account> accounts;

        public Person(string firstname, string lastname)
        {
            this.firstname = firstname;
            this.lastname = lastname;

            accounts = new List<Account>();
        }

        public int GetNewAccountID()
        {
            int retVal = nextAccountID;
            nextAccountID++;

            return retVal;
        }

        /**
         * @brief This method will attempt to withdraw money from the customers accounts.
         * If the customer does not have enough total balance across his or hers accounts
         * an exception will be thrown.
         * 
         * If the customer does not have alle the needed funds in a single account the
         * method will withdraw the amount from several accounts.
         * 
         */
        public void Withdraw(Money amount)
        {
            if (this.GetTotalBalance().CompareTo(amount) == -1)
            {
                throw new BankException("Insufficient funds.");
            }

            Money remaining = new Money(amount);

            foreach (Account account in accounts)
            {
                // If the account has less or equal amount withdraw all of it.
                if (account.GetBalance().CompareTo(remaining) <= 0)
                {
                    Money withdrawAmount = new Money(account.GetBalance());
                    account.Withdraw(withdrawAmount);
                    remaining.Subtract(withdrawAmount);
                }

                else
                {
                    account.Withdraw(remaining);
                    remaining.Subtract(remaining);
                }

                if (remaining.Amount == 0)
                {
                    break;
                }
            }
        }

        public string GetFirstName()
        {
            return firstname;
        }

        public string GetLastName()
        {
            return lastname;
        }

        public void AddAccount(Account account)
        {
            accounts.Add(account);
        }

        public Money GetTotalBalance()
        {
            Money sum = new Money(0);

            foreach (Account account in accounts)
            {
                sum.Add(account.GetBalance());
            }

            return sum;
        }

        public Account[] GetAccounts()
        {
            return accounts.ToArray();
        }
    }
}
