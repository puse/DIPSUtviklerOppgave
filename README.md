Har implementer enhetstester for å vise at programmet gjør som det skal.
Jeg gjorde en antagelse av oppgavebeskrivelsen når det kom til depositumet når
en ny konto opprettes. Om personen ikke har nok penger på 1 konto men har nok
totalt over flere kontoer, så vil pengene trekkes fra flere kontoer.

## Spørsmål

Om dere gir en tilbakemelding på noen av disse spørsmålene så kan jeg endre besvarelsen min.
Det kommer ikke noen feilmeldinger eller lignede opp om disse situasjonene oppstår nå,
men om dere vil at jeg skal legge til den funksjonaliteten som bare gi meg et svar per epost.

Deposit:

2. Hva skal skje om kontoen som det trekkes fra er den samme som det skal settes innpå?

Transfer:

1. Hva skal gjøres om begge kontoer er den samme?
