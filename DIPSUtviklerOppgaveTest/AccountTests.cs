﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DIPSUtviklerOppgave;

namespace DIPSUtviklerOppgaveTest
{
    [TestClass]
    public class AccountTests
    {
 
        [TestMethod]
        public void Account_Withdraw_ValidAmount()
        {
            Money deposit = new Money("100");
            Account account = new Account(null, deposit, "sindre.smistad-1", 1);

            account.Withdraw(new Money("50"));

            Money expected = new Money("50");
            Money actual = account.GetBalance();
            Assert.AreEqual(expected.Amount, actual.Amount, "Amount not withdrawn correctly.");
        }

        [TestMethod]
        public void Account_Withdraw_NegativeAmount()
        {
            Money deposit = new Money("100");
            Account account = new Account(null, deposit, "sindre.smistad-1", 1);

            try
            {
                account.Withdraw(new Money("-50"));
            }
            catch (BankException e)
            {
                StringAssert.Contains(e.Message, BankException.AmountLessOrEqualToZeroMessage);
            }
        }

        [TestMethod]
        public void Account_Withdraw_ZeroAmount()
        {
            Money deposit = new Money("100");
            Account account = new Account(null, deposit, "sindre.smistad-1", 1);

            try
            {
                account.Withdraw(new Money("0"));
            }
            catch (BankException e)
            {
                StringAssert.Contains(e.Message, BankException.AmountLessOrEqualToZeroMessage);
            }
        }

        [TestMethod]
        public void Account_Withdraw_InsufficientAmount()
        {
            Money deposit = new Money("100");
            Account account = new Account(null, deposit, "sindre.smistad-1", 1);

            try
            {
                account.Withdraw(new Money("200"));
            }
            catch (BankException e)
            {
                StringAssert.Contains(e.Message, BankException.InsufficientFundsMessage);
            }
        }


        [TestMethod]
        public void Account_Deposit_ValidAmount()
        {
            Money initialDeposit = new Money("100");
            Account account = new Account(null, initialDeposit, "sindre.smistad-1", 1);

            account.Deposit(new Money("50"));

            Money expected = new Money("150");
            Money actual = account.GetBalance();
            Assert.AreEqual(expected.Amount, actual.Amount, "Account not debited correctly.");
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Account_Deposit_NegativeAmount()
        {
            Money initialDeposit = new Money("100");
            Account account = new Account(null, initialDeposit, "sindre.smistad-1", 1);

            account.Deposit(new Money("-50"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Account_Deposit_ZeroAmount()
        {
            Money initialDeposit = new Money("100");
            Account account = new Account(null, initialDeposit, "sindre.smistad-1", 1);

            account.Deposit(new Money("0"));
        }


        [TestMethod]
        public void Account_Deposit_CompareToLess()
        {
            Money initialDeposit = new Money("100");
            Account account = new Account(null, initialDeposit, "sindre.smistad-1", 1);
            Money otherDeposit = new Money("200");
            Account otherAccount = new Account(null, otherDeposit, "ola.nordmann-1", 1);

            int actual = account.CompareTo(otherAccount);
            int expected = -1;

            Assert.AreEqual(expected, actual, "CompareTo not working correctly.");
        }

        [TestMethod]
        public void Account_Deposit_CompareToGreater()
        {
            Money initialDeposit = new Money("200");
            Account account = new Account(null, initialDeposit, "sindre.smistad-1", 1);
            Money otherDeposit = new Money("100");
            Account otherAccount = new Account(null, otherDeposit, "ola.nordmann-1", 1);

            int actual = account.CompareTo(otherAccount);
            int expected = 1;

            Assert.AreEqual(expected, actual, "CompareTo not working correctly.");
        }

        [TestMethod]
        public void Account_Deposit_CompareToEqual()
        {
            Money initialDeposit = new Money("100");
            Account account = new Account(null, initialDeposit, "sindre.smistad-1", 1);
            Money otherDeposit = new Money("100");
            Account otherAccount = new Account(null, otherDeposit, "ola.nordmann-1", 1);

            int actual = account.CompareTo(otherAccount);
            int expected = 0;

            Assert.AreEqual(expected, actual, "CompareTo not working correctly.");
        }
    }
}
