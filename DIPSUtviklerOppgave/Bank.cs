﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIPSUtviklerOppgave
{
    public class Bank
    {
        /**
         * @breif This method will create a new account for the customer. 
         * The account name will be in the format firstname.lastname-accountid.
         * The account ID is a incrementing number of the accounts belonging to the
         * customer. If the customer dows not have sufficient funds to cover the
         * initial deposit for the account, an exception will be thrown.
         * 
         */
        public Account CreateAccount(Person customer, Money initialDeposit)
        {
            int accountID = customer.GetNewAccountID();
            string accountName = string.Format("{0}.{1}-{2}", customer.GetFirstName(), customer.GetLastName(), accountID);

            if (customer.GetTotalBalance().CompareTo(initialDeposit) < 0)
            {
                throw new BankException(BankException.InsufficientFundsMessage);
            }

            if (initialDeposit.Amount != 0)
            {
                customer.Withdraw(initialDeposit);
            }
            
            Account account = new Account(customer, initialDeposit, accountName, accountID);
            customer.AddAccount(account);

            return account;
        }

        public Account[] GetAccountsForCustomer(Person customer)
        {
            return customer.GetAccounts();
        }

        /**
         * @brief This method will deposit the given amount into the given account if the
         * customer has sufficient funds. If not an exception is thrown.
         * 
         * No solution was given to the case where the money ends up being withdrawn from
         * the same account as it will be deposited to.
         * 
         */
        public void Deposit(Account to, Money amount)
        {
            // Check if customer has enough money.
            if (to.Owner.GetTotalBalance().CompareTo(amount) < 0)
            {
                throw new BankException(BankException.InsufficientFundsMessage);
            }

            to.Deposit(amount);
        }

        public void Withdraw(Account from, Money amount)
        {
            from.Withdraw(amount);
        }

        public void Transfer(Account from, Account to, Money amount)
        {
            from.Withdraw(amount);
            to.Deposit(amount);
        }
    }
}
