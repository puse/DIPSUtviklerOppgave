﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIPSUtviklerOppgave
{
    public class BankException : Exception
    {
        public const string InsufficientFundsMessage = "Insufficient funds.";
        public const string AmountLessOrEqualToZeroMessage = "Cannot withdraw a negative, and or zero amount.";

        public BankException()
        {

        }

        public BankException(string message)
        : base(message)
        {
        }

        public BankException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
