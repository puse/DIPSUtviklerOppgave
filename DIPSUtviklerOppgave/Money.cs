﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIPSUtviklerOppgave
{
    public class Money : IComparable<Money>
    {
        public decimal Amount;

        public Money()
        {

        }

        public Money(decimal amount)
        {
            this.Amount = amount;
        }

        public Money(string amount)
        {
            this.Amount = decimal.Parse(amount);
        }

        public Money(Money money)
        {
            this.Amount = money.Amount;
        }

        public int CompareTo(Money other)
        {
            return this.Amount.CompareTo(other.Amount);
        }

        public void Add(Money amount)
        {
            this.Amount += amount.Amount;
        }

        public void Subtract(Money withdrawAmount)
        {
            this.Amount -= withdrawAmount.Amount;
        }
    }
}
