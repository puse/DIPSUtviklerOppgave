﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DIPSUtviklerOppgave;

namespace DIPSUtviklerOppgaveTest
{
    [TestClass]
    public class BankTests
    {
        [TestMethod]
        public void Bank_CreateAccount_Valid()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();
            Money initialDeposit = new Money("50");

            Account other = bank.CreateAccount(person, initialDeposit);

            Money totalBalanceExpected = new Money("100");
            Money totalBalanceActual = person.GetTotalBalance();

            string accountNameExpected = "sindre.smistad-2";
            string accountNameActual = other.GetAccountName();

            int numberOfAccountsExpected = 2;
            int numberOfAccountsActual = person.GetAccounts().Length;

            Assert.AreEqual(totalBalanceExpected.Amount, totalBalanceActual.Amount, "Balance is not correct.");
            Assert.AreEqual(numberOfAccountsExpected, numberOfAccountsActual, "Number of accounts is incorrect.");
            StringAssert.Contains(accountNameExpected, accountNameActual);
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_CreateAccount_Insufficient()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();
            Money initialDeposit = new Money("500");

            Account other = bank.CreateAccount(person, initialDeposit);
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_CreateAccount_Negative()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();
            Money initialDeposit = new Money("-500");

            Account other = bank.CreateAccount(person, initialDeposit);
        }

        [TestMethod]
        public void Bank_CreateAccount_Zero()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();
            Money initialDeposit = new Money("0");

            Account other = bank.CreateAccount(person, initialDeposit);

            Money balanceExpected = new Money("100");
            Money balanceActual = person.GetTotalBalance();

            Assert.AreEqual(balanceExpected.Amount, balanceExpected.Amount, "Balance is not correct.");
        }

        [TestMethod]
        public void Bank_Deposit_Valid()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Deposit(account, new Money("100"));

            Money balanceExpected = new Money("200");
            Money balanceActual = person.GetTotalBalance();

            Assert.AreEqual(balanceExpected.Amount, balanceExpected.Amount, "Balance is not correct.");
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Deposit_Insufficient()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Deposit(account, new Money("200"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Deposit_Negative()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Deposit(account, new Money("-100"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Deposit_Zero()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Deposit(account, new Money("0"));
        }

        [TestMethod]
        public void Bank_Withdraw_Valid()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Withdraw(account, new Money("50"));

            Money balanceExpected = new Money("50");
            Money balanceActual = person.GetTotalBalance();

            Assert.AreEqual(balanceExpected.Amount, balanceExpected.Amount, "Balance is not correct.");
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Withdraw_Insufficient()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Withdraw(account, new Money("200"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Withdraw_Negative()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Withdraw(account, new Money("-100"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Withdraw_Zero()
        {
            // Need to create a initial account.
            Person person = new Person("sindre", "smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);
            person.GetNewAccountID(); // increment account id.
            Bank bank = new Bank();

            bank.Withdraw(account, new Money("0"));
        }

        [TestMethod]
        public void Bank_Transfer_Valid()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("25");
            Money otherDeposit = new Money("25");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            Account otherAccount = new Account(person, otherDeposit, "sindre.smistad-2", 2);
            person.AddAccount(account);
            person.AddAccount(otherAccount);
            Bank bank = new Bank();

            bank.Transfer(account, otherAccount, new Money("25"));

            Money balaceTotalExpected = new Money("50");
            Money balaceTotalActual = person.GetTotalBalance();

            Money balanceAccountExpected = new Money("0");
            Money balanceAccountActual = account.GetBalance();

            Money balanceOtherAccountExpected = new Money("50");
            Money balanceOtherAccountActual = otherAccount.GetBalance();

            Assert.AreEqual(balaceTotalExpected.Amount, balaceTotalActual.Amount, "Balance is not correct.");
            Assert.AreEqual(balanceAccountExpected.Amount, balanceAccountActual.Amount, "Balance is not correct.");
            Assert.AreEqual(balanceOtherAccountExpected.Amount, balanceOtherAccountActual.Amount, "Balance is not correct.");
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Transfer_Insufficient()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("25");
            Money otherDeposit = new Money("25");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            Account otherAccount = new Account(person, otherDeposit, "sindre.smistad-2", 2);
            person.AddAccount(account);
            person.AddAccount(otherAccount);
            Bank bank = new Bank();

            bank.Transfer(account, otherAccount, new Money("250"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Transfer_Negative()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("25");
            Money otherDeposit = new Money("25");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            Account otherAccount = new Account(person, otherDeposit, "sindre.smistad-2", 2);
            person.AddAccount(account);
            person.AddAccount(otherAccount);
            Bank bank = new Bank();

            bank.Transfer(account, otherAccount, new Money("-250"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Bank_Transfer_Zero()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("25");
            Money otherDeposit = new Money("25");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            Account otherAccount = new Account(person, otherDeposit, "sindre.smistad-2", 2);
            person.AddAccount(account);
            person.AddAccount(otherAccount);
            Bank bank = new Bank();

            bank.Transfer(account, otherAccount, new Money("0"));
        }
    }
}
