﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DIPSUtviklerOppgave;

namespace DIPSUtviklerOppgaveTest
{
    [TestClass]
    public class MoneyTests
    {
        [TestMethod]
        public void Money_CompareTo_Less()
        {
            Money money = new Money("50");
            Money other = new Money("100");

            int actual = money.CompareTo(other);
            int expected = -1;

            Assert.AreEqual(expected, actual, "CompareTo not working correctly.");
        }

        [TestMethod]
        public void Money_CompareTo_Greater()
        {
            Money money = new Money("500");
            Money other = new Money("100");

            int actual = money.CompareTo(other);
            int expected = 1;

            Assert.AreEqual(expected, actual, "CompareTo not working correctly.");
        }

        [TestMethod]
        public void Money_CompareTo_Equal()
        {
            Money money = new Money("100");
            Money other = new Money("100");

            int actual = money.CompareTo(other);
            int expected = 0;

            Assert.AreEqual(expected, actual, "CompareTo not working correctly.");
        }
    }
}
