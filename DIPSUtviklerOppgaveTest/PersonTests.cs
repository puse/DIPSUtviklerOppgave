﻿/**
 * Sindre Smistad
 * 
 *  For DIPS ASA
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DIPSUtviklerOppgave;

namespace DIPSUtviklerOppgaveTest
{
    [TestClass]
    public class PersonTests
    {
        [TestMethod]
        public void Person_GetNewAccountID_StartsAt1()
        {
            Person person = new Person("Sindre", "Smistad");

            int actual = person.GetNewAccountID();
            int expected = 1;

            Assert.AreEqual(actual, expected, "Account Id does not start with 1.");
        }

        [TestMethod]
        public void Person_GetNewAccountID_Increments()
        {
            Person person = new Person("Sindre", "Smistad");

            person.GetNewAccountID(); // Should increment
            int actual = person.GetNewAccountID();
            int expected = 2;

            Assert.AreEqual(actual, expected, "Account Id does not increment.");
        }

        [TestMethod]
        public void Person_GetTotalBalance_SingleAccount()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);

            Money actual = person.GetTotalBalance();
            Money expected = new Money("100");

            Assert.AreEqual(actual.Amount, expected.Amount, "Balance not correct.");
        }

        [TestMethod]
        public void Person_GetTotalBalance_MultipleAccounts()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            Account otherAccount = new Account(person, deposit, "sindre.smistad-2", 2);
            person.AddAccount(account);
            person.AddAccount(otherAccount);

            Money actual = person.GetTotalBalance();
            Money expected = new Money("200");

            Assert.AreEqual(actual.Amount, expected.Amount, "Balance not correct.");
        }

        [TestMethod]
        public void Person_Withdraw_ValidAmountSingleAccount()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);

            person.Withdraw(new Money("50"));

            Money actual = person.GetTotalBalance();
            Money expected = new Money("50");

            Assert.AreEqual(expected.Amount, actual.Amount, "Amount not withdrawn correctly.");
        }

        [TestMethod]
        public void Person_Withdraw_ValidAmountMultipleAccounts()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("25");
            Money otherDeposit = new Money("25");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            Account otherAccount = new Account(person, otherDeposit, "sindre.smistad-2", 2);
            person.AddAccount(account);
            person.AddAccount(otherAccount);

            person.Withdraw(new Money("50"));

            Money actual = person.GetTotalBalance();
            Money expected = new Money("0");

            Assert.AreEqual(expected.Amount, actual.Amount, "Amount not withdrawn correctly.");
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Person_Withdraw_ValidAmountSingleAccount_Insufficient()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("100");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            person.AddAccount(account);

            person.Withdraw(new Money("150"));
        }

        [TestMethod]
        [ExpectedException(typeof(BankException))]
        public void Person_Withdraw_ValidAmountMultipleAccounts_Insufficient()
        {
            Person person = new Person("Sindre", "Smistad");
            Money deposit = new Money("25");
            Money otherDeposit = new Money("25");
            Account account = new Account(person, deposit, "sindre.smistad-1", 1);
            Account otherAccount = new Account(person, otherDeposit, "sindre.smistad-2", 2);
            person.AddAccount(account);
            person.AddAccount(otherAccount);

            person.Withdraw(new Money("150"));
        }
    }
}
